import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";

import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginComponent } from './login/login.component';

import {LoginService} from './services/login.service';
import {CadastroService} from './services/cadastro.service'


import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card'; 
import { BrowserModule } from '@angular/platform-browser';

/**
 *  Modulo responsavel pelos componentes de Cadastro/Login.
 *  Provedores: loginService e cadastroService;
 * 
 * @author osvaldoairon
 */
@NgModule({
  declarations: [CadastroComponent, LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
  ],
  exports: [CadastroComponent,LoginComponent],
  providers: [LoginService,CadastroService],
})
export class CredencialModule { }
