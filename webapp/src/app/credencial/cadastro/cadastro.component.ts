import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import {User} from './models/User';
import {CadastroService} from '../services/cadastro.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  formCreateUser: FormGroup;
  private cadastroService: CadastroService;

  constructor(private formUserBuilder: FormBuilder, private cadService: CadastroService) { 
    this.cadastroService = cadService;
  }

  ngOnInit(): void {
    this.generateForm();
  }

  createUser(){
    let name = this.formCreateUser.value['name'];
    let lastname = this.formCreateUser.value['lastname'];
    let email = this.formCreateUser.value['email'];
    let username = this.formCreateUser.value['username'];
    let password = this.formCreateUser.value['password'];
    let confirm_password = this.formCreateUser.value['confirm_password'];

    if(password !== confirm_password){
      alert('Senhas não conferem.')
    }else{
      this.cadastroService.createUser(new User(name,lastname,email,username,password));
    }
  }
  generateForm(){
    this.formCreateUser = this.formUserBuilder.group({
      name:[''],
      lastname:[''],
      email:[''],
      username:[''],
      password:[''],
      confirm_password:['']
    })
  }
}
