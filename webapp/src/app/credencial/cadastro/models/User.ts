
export class User{

    name: string
    lastName: string
    email: string
    username: string
    password: string

    constructor(name: string, lastName: string, email: string, username: string , password: string){
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
    }
}