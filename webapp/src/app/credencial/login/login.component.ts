import { Component, OnInit } from '@angular/core';
import{LoginService} from '../services/login.service';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import { UserAuth } from './models/UserAuth';

/**
 * Componente responsavel pela autenticação do usuário. 
 * @author osvaldoairon
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formUser: FormGroup;

  constructor(private loginService: LoginService, private formUserBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.generateForm();
  }

  authenticateUser(): void {
    let username = this.formUser.value['username'];
    let password = this.formUser.value['password'];
    let userAuth = this.loginService.createUserAuth(username,password);
    if(userAuth !== null){
      this.loginService.autenticateUser(userAuth);
    }
  }
  generateForm(){
    this.formUser = this.formUserBuilder.group({
      username:[''],
      password:[''],
    })
  }
  actionButton(): void {
    console.log("sender data ok");
  }

}
