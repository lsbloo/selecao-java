import { Injectable } from '@angular/core';
import {User} from '../cadastro/models/User';
import {ApiService} from './api.service';


@Injectable({
  providedIn: 'root'
})
export class CadastroService {
  private user: User;

  constructor(private apiService: ApiService) { }


  createModelUser(name: string, lastName: string, email: string,username: string, password: string): User{
    if(username === null || password === null || email === null || name === null)return ;
    return new User(name,lastName,email,username,password);
  }

  createUser(user: User): any {
    console.log(this.apiService.createUser(user));
  }

}
