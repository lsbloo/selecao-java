/**
 *  Serviço de conexão com api-service;
 * @author osvaldo.airon
 */
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of, throwError, observable } from 'rxjs';
import { UserAuth } from '../login/models/UserAuth';
import { catchError, retry } from 'rxjs/operators'
import { from } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../cadastro/models/User';
import {Historico} from '../../dashboard/historico/models/Historico';
import { error } from '@angular/compiler/src/util';
import { THIS_EXPR, ArrayType } from '@angular/compiler/src/output/output_ast';
import {Usuario} from '../../dashboard/usuarios/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  /**
   * Constantes de urls e cabeçalhos.
   */
  static readonly URL_HOST="/api/v1/";
  static readonly URL_LOGIN=ApiService.URL_HOST + "login";
  
  static readonly URL_CREATE_USER= ApiService.URL_HOST + "user/";

  static readonly URL_IMPORT_CSV = ApiService.URL_HOST + "import";

  static readonly URL_RESOURCE_DATA = ApiService.URL_HOST + "resource/data";

  static readonly URL_RESOURCE_DISTRIBUTOR = ApiService.URL_HOST + "resource/distributor";

  static readonly URL_RESOURCE_INITIALS = ApiService.URL_HOST + "resource/initials";
  static readonly URL_RESOURCE_PRICE_CITY = ApiService.URL_HOST + "resource/price";
  static readonly URL_RESOURCE_CITY = ApiService.URL_HOST + "resource/price/city";
  static readonly URL_RESOURCE_FLAG = ApiService.URL_HOST + "resource/price/flag";

  static readonly URL_HISTORY_CRUD = ApiService.URL_HOST + "history";
  static readonly URL_USER_CRUD = ApiService.URL_HOST + "user";

  static readonly URL_USER_FIND_ALL = ApiService.URL_HOST + "user/all";



  static readonly httpOption = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }
  senderBadRequest = new EventEmitter<string>();
  senderBadRequestOperationDeleteHistory = new EventEmitter<string>();
  senderBadRequestOperationGetHistory = new EventEmitter<string>();
  senderBadRequestOperationUpdateHistory = new EventEmitter<string>();


  senderToken = new EventEmitter<string>();
  senderConfirmationCsv = new EventEmitter<string>();
 
  senderInformationAditionalParamQuery = new EventEmitter<String[]>();
  senderHistorysParamQuery = new EventEmitter<Historico[]>();
  senderUsersParamQuery = new EventEmitter<Usuario[]>();

  senderResult = new EventEmitter<string>();


  constructor(private http: HttpClient, private router: Router) { }


  /**
   * realiza o processo de autenticação do usuário
   * esperado um token bearer de acesso.
   * @param userAuth 
   * @returns any
   */
  authenticate(userAuth: UserAuth): void {
    let url_login = ApiService.URL_LOGIN;
    const headers = new HttpHeaders().set('Content-Type', 'application/json;')
    this.http.post(url_login,{"username": userAuth.username, "password": userAuth.password},{headers,responseType: 'text'})
    .subscribe(data => {
      let token = data.substring(10,data.length -2);
      localStorage.setItem(userAuth.username, token);
      this.router.navigate(['/painel'], {state: {string: userAuth.username}});

      this.senderToken.emit(userAuth.username);

    });
  }

  deleteUser(id_user: number, token_user){
    let url = ApiService.URL_USER_CRUD;
    const headers = new HttpHeaders().set('Content-Type', 'application/json;')
    .set('Authorization', token_user);
    let paramsx = new HttpParams();
    paramsx = paramsx.append('id', id_user.toString())

    this.http.delete(url,{headers: headers, observe: 'response', params: paramsx}).subscribe(response => {

      if(response.status === 202){
        this.senderResult.emit('user_delete');
      }
    }, () => {
      this.senderBadRequest.emit('bad_request');
    });
  }

  findAllUser(token_user: string) {
    let url = ApiService.URL_USER_FIND_ALL;
    const headers = new HttpHeaders().set('Content-Type', 'application/json;')
    .set('Authorization', token_user);

    let arrayUsers = new Array<Usuario>();

    this.http.get(url,{headers: headers, observe: 'response'}).subscribe(response => {
        if(response.status === 202){
          

          if(Array.isArray(response.body)){

          for (let index = 0; index < response.body.length; index++) {
            const element = response.body[index];
          

            arrayUsers.push(new Usuario(element['name'], element['username'],element['email'],element['id'],element['lastName']))
            
          }
        }
        this.senderUsersParamQuery.emit(arrayUsers);
      }

    }, () => {
      this.senderBadRequest.emit('bad_request');
    });
  }
  createUser(user: User): void{
    let url_create_user = ApiService.URL_CREATE_USER;
    const headers = new HttpHeaders().set('Content-Type', 'application/json;')
    this.http.post(url_create_user,{"name": user.name, "lastName": user.lastName
  ,"email": user.email, "username": user.username, "password": user.password},{headers, observe: 'response'})
    .subscribe(response => {

      if (response.status === 201){
        this.router.navigate(['/login']);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
  }

  importCsvUser(token_user: string, file_csv: FormData): boolean {
    let url_import_csv = ApiService.URL_IMPORT_CSV;
    const headers = new HttpHeaders()
    .set('Authorization', token_user);


    this.http.post(url_import_csv,file_csv,{headers, observe: 'response'}).subscribe(
      response => {
        if(response.status === 202){
          this.senderConfirmationCsv.emit('import_ok');
        }
        return false;
      },() => {
        this.senderBadRequest.emit('bad_request');
      });

    return false; 
  }

  getResultParamData(search: string, page: number,param: string, token_user: string,param_type: string): Observable<Historico[]>{
    let url = ApiService.URL_RESOURCE_DATA;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let params = new HttpParams();
    let size = '20';

    let arrayHistorico = new Array<Historico>();
    let arrayInf = new Array<String>();


    params = params.append('data', search);
    params = params.append('type', param_type);
    params = params.append('page', page.toString());
    params = params.append('size', size);
    this.http.get<Historico[]>(url,{headers, observe: 'response', params: params})
    .subscribe(response =>{
      if(response.status === 202){
        arrayInf.push(response.body['totalElements']);
        arrayInf.push(response.body['totalPages']);

        response.body['content'].forEach(element => {
          arrayHistorico.push(new Historico(element['id'],element['type'],element['codigo_instalacao'],
          element['data_coleta'],element['estado'],element['municipio'],element['produto'],
          element['regiao'], element['revenda'], element['unidade_medida'],element['bandeira'],
          element['valor_venda'], element['valor_compra'])
          )
        });
        this.senderHistorysParamQuery.emit(arrayHistorico);
        this.senderInformationAditionalParamQuery.emit(arrayInf);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
    return null;
  }

  getResultParamRevenda(search: string, page: number,param: string, token_user: string,param_type: string): Observable<Historico[]>{
    let url = ApiService.URL_RESOURCE_DISTRIBUTOR;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let params = new HttpParams();
    let size = '20';

    let arrayHistorico = new Array<Historico>();
    let arrayInf = new Array<String>();


    params = params.append('distribuidora', search);
    params = params.append('type', param_type);
    params = params.append('page', page.toString());
    params = params.append('size', size);
    this.http.get<Historico[]>(url,{headers, observe: 'response', params: params})
    .subscribe(response =>{
      if(response.status === 202){
        arrayInf.push(response.body['totalElements']);
        arrayInf.push(response.body['totalPages']);

        response.body['content'].forEach(element => {
          arrayHistorico.push(new Historico(element['id'],element['type'],element['codigo_instalacao'],
          element['data_coleta'],element['estado'],element['municipio'],element['produto'],
          element['regiao'], element['revenda'], element['unidade_medida'],element['bandeira'],
          element['valor_venda'], element['valor_compra'])
          )
        });
        this.senderHistorysParamQuery.emit(arrayHistorico);
        this.senderInformationAditionalParamQuery.emit(arrayInf);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
    return null;
  }
  
  getResultParamFlag(search: string,param: string, token_user: string, param_type: string): Observable<Historico[]>{
    let url = ApiService.URL_RESOURCE_FLAG;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let params = new HttpParams();
    
    params = params.append('bandeira', search);
    params = params.append('type', param_type);

    this.http.get(url,{headers, observe: 'response', params: params})
    .subscribe(response =>{
      if(response.status === 202){
        this.senderInformationAditionalParamQuery.emit([response.body['media_price_compra'], response.body['media_price_venda']]);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
    return null;
  }
  getResultParamCity(search: string,param: string, token_user: string, param_type: string): Observable<Historico[]>{
    let url = ApiService.URL_RESOURCE_CITY;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let params = new HttpParams();
    
    params = params.append('municipio', search);
    params = params.append('type', param_type);

    this.http.get(url,{headers, observe: 'response', params: params})
    .subscribe(response =>{
      if(response.status === 202){
        this.senderInformationAditionalParamQuery.emit([response.body['media_price_compra'], response.body['media_price_venda']]);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
    return null;
  }

  getResultParamPriceCity(search: string,param: string, token_user: string, param_type: string): Observable<Historico[]>{
    let url = ApiService.URL_RESOURCE_PRICE_CITY;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let params = new HttpParams();
    
    params = params.append('nome_municipio', search);
    params = params.append('type', param_type);

    this.http.get(url,{headers, observe: 'response', params: params})
    .subscribe(response =>{
      if(response.status === 202){
        this.senderInformationAditionalParamQuery.emit(response.body['price']);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
    return null;
  }

  getResultParamSigla(search: string, page: number,param: string, token_user: string, param_type: string): Observable<Historico[]>{
    let url = ApiService.URL_RESOURCE_INITIALS;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let params = new HttpParams();
    let size = '20';

    let arrayHistorico = new Array<Historico>();
    let arrayInf = new Array<String>();


    params = params.append('sigla', search);
    params = params.append('type', param_type);
    params = params.append('page', page.toString());
    params = params.append('size', size);
    this.http.get<Historico[]>(url,{headers, observe: 'response', params: params})
    .subscribe(response =>{
      if(response.status === 202){
        arrayInf.push(response.body['totalElements']);
        arrayInf.push(response.body['totalPages']);

        response.body['content'].forEach(element => {
          arrayHistorico.push(new Historico(element['id'],element['type'],element['codigo_instalacao'],
          element['data_coleta'],element['estado'],element['municipio'],element['produto'],
          element['regiao'], element['revenda'], element['unidade_medida'],element['bandeira'],
          element['valor_venda'], element['valor_compra'])
          )
        });
        this.senderHistorysParamQuery.emit(arrayHistorico);
        this.senderInformationAditionalParamQuery.emit(arrayInf);
      }
    },() => {
      this.senderBadRequest.emit('bad_request');
    });
    return null;
  }

  createHistory(history: Historico, token_user: string){
    let url = ApiService.URL_HISTORY_CRUD;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    this.http.post(url,history,{headers: headers, observe: 'response'}).subscribe(response =>{

      if(response.status === 201 || response.status === 202){
        this.senderResult.emit('create_history')
      }
    }, () => {

      this.senderBadRequest.emit('bad_request');
    });
  }

  deleteHistory(id_history: number, token_user: string){
    let url = ApiService.URL_HISTORY_CRUD;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let paramsx = new HttpParams();
    paramsx = paramsx.append('id_history', id_history.toString());

    this.http.delete(url,{headers, observe: 'response', params: paramsx}).subscribe(response => {

      if(response.status === 202){
        this.senderResult.emit('delete_history')
      }
    }, () => {
      this.senderBadRequestOperationDeleteHistory.emit('bad_request');
    });

  }

  editHistory(historico: Historico, token_user: string, id_history: number){
    let url = ApiService.URL_HISTORY_CRUD;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let paramsx = new HttpParams();
    paramsx = paramsx.append('id_history', id_history.toString());

    this.http.put(url,historico, {headers, observe: 'response', params: paramsx}).subscribe(response =>{

      if(response.status === 202){
        this.senderResult.emit('update_history')
      }
    },() => {
      this.senderBadRequestOperationGetHistory.emit('bad_request');
    });
  }
  getHistory(id_history: number , token_user: string){

    let url = ApiService.URL_HISTORY_CRUD;
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', token_user);
    let paramsx = new HttpParams();
    paramsx = paramsx.append('id_history', id_history.toString());
    let arrayHistorico = new Array<Historico>();


    this.http.get(url,{headers, observe: 'response', params: paramsx}).subscribe(response =>{

      if(response.status === 202){
        let history = response.body['history'];
          arrayHistorico.push(new Historico(history.id,history.type,history.codigo_instalacao,
          history.data_coleta,history.estado,history.municipio,history.produto,
          history.regiao, history.revenda, history.unidade_medida,history.bandeira,
          history.valor_venda, history.valor_compra)
          )
        
        this.senderHistorysParamQuery.emit(arrayHistorico);
      }
    }, () => {
      this.senderBadRequestOperationGetHistory.emit('bad_request');
    });
  }
}
