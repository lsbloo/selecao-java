import { Injectable } from '@angular/core';
import {UserAuth} from '../login/models/UserAuth';
import {ApiService} from './api.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private userAuth: UserAuth;

  constructor(private apiService: ApiService) { }


  createUserAuth(username: string, password: string): UserAuth{
    if(username === null || password === null)return ;
    return new UserAuth(username,password);
  }

  autenticateUser(userAuth: UserAuth): any {
    console.log(this.apiService.authenticate(userAuth));
  }

}
