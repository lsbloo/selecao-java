import { Component, OnInit } from '@angular/core';
import { PainelService } from '../services/painel.service';
import { Router } from '@angular/router';
import { IfStmt } from '@angular/compiler';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';


@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit {

  username: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(private breakpointObserver: BreakpointObserver, private painelService: PainelService, public router: Router) {
  
    
    try {
      const nav = router.getCurrentNavigation();
      if(typeof nav != "undefined"){
        let u = nav.extras.state.string;
        let token_user = localStorage.getItem(u);
        this.username = u;
        router.navigate(['/painel/historico']);
      }
    } catch (error) {
      alert("Pagina não autorizada!, realize o login");
      router.navigate(['/'])
    } 
  }


  ngOnInit(): void {
  }
  getDashUser(): boolean  {
    console.log("click");    return true;
  }
}
