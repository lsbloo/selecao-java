
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import {TooltipPosition} from '@angular/material/tooltip';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { SelectionModel } from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {ApiService} from '../../credencial/services/api.service';
import { UserService } from '../services/user.service';
import { Usuario } from './models/Usuario';

export interface userTable {
  id: number
  name: string;
  lastName: string;
  email: string;
}

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'sobrenome', 'email', 'actions'];
  dataSource = new MatTableDataSource<userTable>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  
  constructor(private dialog: MatDialog,private userService: UserService,private apiService: ApiService, public router: Router, private formSenderBuilder: FormBuilder) {

    this.apiService.senderToken.subscribe((username => {
      sessionStorage.setItem("username", username);
    }));
  }

  ngOnInit(): void {
    let username = sessionStorage.getItem('username');
    let token_user = localStorage.getItem(username);
    this.findAllUsers(token_user);
    this.apiService.senderUsersParamQuery.subscribe(data => {
      this.dataSource = new MatTableDataSource<userTable>(data);
      this.dataSource.paginator = this.paginator;
    });
  }


  findAllUsers(token_user: string): void {
    this.userService.findAllUsers(token_user);
  }

  deleteUser(element): void {
    this.dataSource.data = this.dataSource.data.filter(i => i !== element)
    let username = sessionStorage.getItem('username');
    let token_user = localStorage.getItem(username);
    this.userService.deleteUser(element['id'], token_user);
    this.apiService.senderResult.subscribe(data => {
      if(data === 'user_delete'){
        console.log('Usuário deletado com sucesso');
      }
    })
  }
}
