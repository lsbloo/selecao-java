

export class Usuario {
    
    name: string
    username: string
    email: string
    id: number
    lastName: string

    constructor(name: string, username: string, email: string, id: number, lastName: string){
        this.name=name
        this.username=username
        this.email=email
        this.id=id
        this.lastName=lastName
    }
}