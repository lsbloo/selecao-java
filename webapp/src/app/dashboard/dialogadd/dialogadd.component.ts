import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import {DialogData} from '../historico/historico.component';
import {ApiService} from '../../credencial/services/api.service';
import { Historico } from '../historico/models/Historico';


@Component({
  selector: 'app-dialogadd',
  templateUrl: './dialogadd.component.html',
  styleUrls: ['./dialogadd.component.css']
})
export class DialogaddComponent implements OnInit {

  formCreateHistory: FormGroup
  constructor(private apiService: ApiService,
    public dialogRef: MatDialogRef<DialogaddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private formBuilder: FormBuilder) {

      this.apiService.senderToken.subscribe((username => {
        sessionStorage.setItem("username", username);
      }));
     }

  ngOnInit(): void {
    this.generateForm();
    }


  createHistory(): void {
    let history = new Historico(null,null,this.formCreateHistory.value['codigo_instalacao'],this.formCreateHistory.value['data_coleta'],
    this.formCreateHistory.value['estado'],this.formCreateHistory.value['municipio'],
    this.formCreateHistory.value['produto'],this.formCreateHistory.value['regiao'],
    this.formCreateHistory.value['revenda'],this.formCreateHistory.value['unidade_medida'],
    this.formCreateHistory.value['bandeira'],this.formCreateHistory.value['valor_venda'],
    this.formCreateHistory.value['valor_compra'])
    
    let username = sessionStorage.getItem('username');
    let token_user = localStorage.getItem(username);

    console.log("Historico" +history);


    this.apiService.createHistory(history,token_user);
    
    this.apiService.senderBadRequest.subscribe(data => {
      if(data === 'bad_request'){
        alert('Ocorreu um erro na criação do histórico, tente novamente!');
      }
    });
    this.apiService.senderResult.subscribe(data => {
      console.log("Data: "  + data)
      if(data === 'create_history'){
        alert('Historico Criado com successo');
      }
    });


  }

  generateForm(): void {
    this.formCreateHistory = this.formBuilder.group({
      regiao:[''],
      bandeira:[''],
      estado:[''],
      municipio:[''],
      produto:[''],
      revenda:[''],
      codigo_instalacao:[''],
      unidade_medida:[''],
      valor_compra:[''],
      valor_venda:[''],
      data_coleta:[''],
    });

  }

}
