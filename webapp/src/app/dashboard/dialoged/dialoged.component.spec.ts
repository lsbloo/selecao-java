import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogedComponent } from './dialoged.component';

describe('DialogedComponent', () => {
  let component: DialogedComponent;
  let fixture: ComponentFixture<DialogedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
