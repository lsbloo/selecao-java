import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import {DialogData} from '../historico/historico.component';
import {ApiService} from '../../credencial/services/api.service';
import { Historico } from '../historico/models/Historico';

@Component({
  selector: 'app-dialoged',
  templateUrl: './dialoged.component.html',
  styleUrls: ['./dialoged.component.css']
})
export class DialogedComponent implements OnInit {

  formEditHistory: FormGroup
  constructor(private apiService: ApiService,
    public dialogRef: MatDialogRef<DialogedComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private formBuilder: FormBuilder) {

      this.apiService.senderToken.subscribe((username => {
        sessionStorage.setItem("username", username);
      }));
     }

  ngOnInit(): void {
    console.log(this.data.estado);
    let username = sessionStorage.getItem('username');
    let token_user = localStorage.getItem(username);
    let id: number = +this.data.estado;
    this.apiService.getHistory(id,token_user);
    this.apiService.senderHistorysParamQuery.subscribe(historyModel => {
      console.log(historyModel);

      this.generateForm(historyModel[0]);
    });
    
  }

  editHistory(): void {
    console.log(this.data);

    let history = new Historico(null,null,this.formEditHistory.value['codigo_instalacao'],this.formEditHistory.value['data_coleta'],
    this.formEditHistory.value['estado'],this.formEditHistory.value['municipio'],
    this.formEditHistory.value['produto'],this.formEditHistory.value['regiao'],
    this.formEditHistory.value['revenda'],this.formEditHistory.value['unidade_medida'],
    this.formEditHistory.value['bandeira'],this.formEditHistory.value['valor_venda'],
    this.formEditHistory.value['valor_compra'])
    
    console.log(history.regiao);
    
    let username = sessionStorage.getItem('username');
    let token_user = localStorage.getItem(username);

    let id: number = +this.data.estado;
    this.apiService.editHistory(history,token_user,id);
    
    this.apiService.senderBadRequestOperationUpdateHistory.subscribe(data => {
      if(data === 'bad_request'){
        alert('Ocorreu um erro na edição do histórico, tente novamente!');
      }
    });
    this.apiService.senderResult.subscribe(data => {
      if(data === 'update_history'){
        alert('Historico Atualizado com successo');
      }
    });

  }

  generateForm(historico_selecionado : Historico): void {
    this.formEditHistory = this.formBuilder.group({
      regiao:[historico_selecionado.regiao],
      bandeira:[historico_selecionado.bandeira],
      estado:[historico_selecionado.estado],
      municipio:[historico_selecionado.municipio],
      produto:[historico_selecionado.produto],
      revenda:[historico_selecionado.revenda],
      codigo_instalacao:[historico_selecionado.codigo_instalacao],
      unidade_medida:[historico_selecionado.unidade_medida],
      valor_compra:[historico_selecionado.valor_compra],
      valor_venda:[historico_selecionado.valor_venda],
      data_coleta:[historico_selecionado.data_coleta],
    });

  }
}
