import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PainelComponent } from './painel/painel.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card'; 
import {MatToolbarModule} from '@angular/material/toolbar';
import {PainelService} from './services/painel.service';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {AppRoutingModule} from '../app-routing.module';
import {MatTableModule} from '@angular/material/table';
import {HistoricoComponent} from './historico/historico.component'; 
import {MatPaginatorModule} from '@angular/material/paginator'; 
import {MatExpansionModule} from '@angular/material/expansion'; 
import {MatSelectModule} from '@angular/material/select'; 
import {MatTooltipModule} from '@angular/material/tooltip'; 
import {MatButtonModule} from '@angular/material/button'; 
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import { HistoricoService } from './services/historico.service';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; 
import {MatDialogModule} from '@angular/material/dialog';
import { DialogaddComponent } from './dialogadd/dialogadd.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { DialogedComponent } from './dialoged/dialoged.component'; 
import {UserService} from './services/user.service';

@NgModule({
  declarations: [PainelComponent, UsuariosComponent, HistoricoComponent,DialogaddComponent, DialogedComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatSelectModule,
    MatTooltipModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatGridListModule,
    AppRoutingModule
  ], 
  exports: [PainelComponent, UsuariosComponent,HistoricoComponent,DialogaddComponent,DialogedComponent],
  providers: [PainelService,HistoricoService, UserService]
})
export class DashboardModule { }
