import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import {TooltipPosition} from '@angular/material/tooltip';
import {ApiService} from '../../credencial/services/api.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import {HistoricoService} from '../services/historico.service';
import { Historico } from './models/Historico';
import { DialogaddComponent } from '../dialogadd/dialogadd.component';
import {MatDialog} from '@angular/material/dialog';
import { SelectionModel } from '@angular/cdk/collections';
import { DialogedComponent } from '../dialoged/dialoged.component';

export interface DialogData{
  codigo_instalacao: string, 
  data_coleta: string,
  estado: string,
  municipio: string, 
  produto: string, 
  regiao: string, 
  revenda: string, 
  unidade_medida: string , 
  bandeira: string, 
  valor_venda: string, 
  valor_compra: string
};
export interface historicoTable {
  id: number
  bandeira: string,
  codigo_instalacao: string,
  data_coleta: string,
  estado: string,
  municipio: string,
  produto: string,
  regiao: string,
  revenda: string,
  unidade_medida: string,
  valor_compra: string,
  valor_venda: string
}


@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css']
})
export class HistoricoComponent implements OnInit {
  panelOpenState = false;

  public token_user: string;

  displayedColumns: string[] = [
    'id', 
    'bandeira',
    'codigo_instalacao',
    'data_coleta',
    'estado',
    'municipio',
    'produto',
    'regiao',
    'revenda',
    'unidade_medida',
    'valor_compra',
    'valor_venda',
    'actions'
  ];
  dataSource = new MatTableDataSource<historicoTable>();


  positionOptions: TooltipPosition[] = ['below', 'above', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  public formSenderParam: FormGroup;
  public file: File;

  @Input()
  showSpinner;

  paginas: string;
  elementos: string;

  media_price_city: string;


  public region: string="";
  public state: string="";


  constructor(private dialog: MatDialog,private historyService: HistoricoService,private apiService: ApiService, public router: Router, private formSenderBuilder: FormBuilder) {
  
    this.apiService.senderToken.subscribe((username => {
      sessionStorage.setItem("username", username);
    }));


  }

  ngOnInit(){
    this.showSpinner = false;
    this.dataSource.paginator = this.paginator;
    this.generator();

  }

  openDialog(){
      const dialogRef = this.dialog.open(DialogaddComponent,
        {data:{regiao: this.region, estado: this.state}});
      
        dialogRef.afterClosed().subscribe(result =>{
        console.log('dialog closed' + result);
      });

  }

  senderSearchByParam(): void {
    let input = this.formSenderParam.value['input']
    let param = this.formSenderParam.value['param']
    let page = this.formSenderParam.value['page']
    let param_type= this.formSenderParam.value['param_type']

    let username = sessionStorage.getItem('username');
    this.token_user = localStorage.getItem(username);
    console.log(param);

    switch(param){
      case "Data de Coleta":
        this.historyService.getResultParamData(input,page,param,this.token_user,param_type);
        this.apiService.senderBadRequest.subscribe(response => {
          if(response === 'bad_request'){
            alert("Erro na busca, por favor checar parametros.");
          }
        });
        this.apiService.senderHistorysParamQuery.subscribe(response => {
        this.dataSource = new MatTableDataSource<historicoTable>(response);
        this.dataSource.paginator = this.paginator;

        });
        this.apiService.senderInformationAditionalParamQuery.subscribe(response =>{
            this.paginas = response[1];
            this.elementos = response[0];
        });
        break;
      case "Revenda":
        this.historyService.getResultParamRevenda(input,page,param,this.token_user,param_type);
        this.apiService.senderBadRequest.subscribe(response => {

          if(response === 'bad_request'){
            alert("Erro na busca, por favor checar parametros.");
          }
        });
        this.apiService.senderHistorysParamQuery.subscribe(response => {
        this.dataSource = new MatTableDataSource<historicoTable>(response);
        this.dataSource.paginator = this.paginator;
        });
        this.apiService.senderInformationAditionalParamQuery.subscribe(response =>{
            this.paginas = response[1];
            this.elementos = response[0];
        });
        break;
      case "Sigla":
        this.historyService.getResultParamSigla(input,page,param,this.token_user,param_type);
        this.apiService.senderBadRequest.subscribe(response => {

          if(response === 'bad_request'){
            alert("Erro na busca, por favor checar parametros.");
          }
        });
        this.apiService.senderHistorysParamQuery.subscribe(response => {
        this.dataSource = new MatTableDataSource<historicoTable>(response);
        this.dataSource.paginator = this.paginator;
        });
        this.apiService.senderInformationAditionalParamQuery.subscribe(response =>{
            this.paginas = response[1];
            this.elementos = response[0];
        });
        break;
      case "Média Municipio":
        this.historyService.getResultParamPriceCity(input,param,this.token_user,param_type);
        this.apiService.senderBadRequest.subscribe(response => {

          if(response === 'bad_request'){
            alert("Erro na busca, por favor checar parametros.");
          }
        });
        this.apiService.senderInformationAditionalParamQuery.subscribe(response =>{
            this.media_price_city = response;
            alert("A media de preço do combustivel é: " + this.media_price_city);
        });
        break;
      case "Municipio":
        this.historyService.getResultParamCity(input,param,this.token_user,param_type);
        this.apiService.senderBadRequest.subscribe(response => {

          if(response === 'bad_request'){
            alert("Erro na busca, por favor checar parametros.");
          }
        });
        this.apiService.senderInformationAditionalParamQuery.subscribe(response =>{
            this.media_price_city = response;
            alert("A media do valor de compra do combustivel é: " + this.media_price_city[0]
                 + "\nA media do valor de venda do combustivel é: " + this.media_price_city[1]);
        });
        break;
      case "Bandeira":
        this.historyService.getResultParamFlag(input,param,this.token_user,param_type);
        this.apiService.senderBadRequest.subscribe(response => {

          if(response === 'bad_request'){
            alert("Erro na busca, por favor checar parametros.");
          }
        });
        this.apiService.senderInformationAditionalParamQuery.subscribe(response =>{
            this.media_price_city = response;
            alert("A media do valor de compra do combustivel é: " + this.media_price_city[0]
                 + "\nA media do valor de venda do combustivel é: " + this.media_price_city[1]);
        });
        break;

      default:
        param = 0;
      
    }
  }
  senderCsv(): void {}

  generator(): void {
    this.formSenderParam = this.formSenderBuilder.group({
      input:[''],
      param:[''],
      param_type:[''],
      page:['']
    });
  }


  public changeListener(files: FileList): void {
    if(files && files.length > 0) {
      let file : File = files.item(0); 
        this.file = file;
    }
    let formData = new FormData();
    formData.append('archive',this.file,this.file.name);
    
    let username = sessionStorage.getItem('username');
    this.token_user = localStorage.getItem(username);
  
    this.historyService.addCsv(this.token_user,formData);
    
    this.apiService.senderBadRequest.subscribe(response => {
      if(response === 'bad_request'){
        alert('Ocorreu um erro na importação, tente novamente!')
      }
    });
    this.apiService.senderConfirmationCsv.subscribe(response => {
      if(response === "import_ok"){
        alert("Importação Realizada com sucesso!");
      }
    });
    
  }

  updateHistory(element): void {
    console.log(element['id']);

    const dialogRef = this.dialog.open(DialogedComponent,
      {data:{ estado: element['id']}});
      dialogRef.afterClosed().subscribe(result =>{
        this.dataSource = new MatTableDataSource<historicoTable>(null);
        this.dataSource.paginator = this.paginator;
    });

  }
  deleteHistory(element): void {
    this.dataSource.data = this.dataSource.data.filter(i => i !== element)
    let username = sessionStorage.getItem('username');
    this.token_user = localStorage.getItem(username);
    this.historyService.deleteHistory(element['id'], this.token_user);
    this.apiService.senderBadRequestOperationDeleteHistory.subscribe(data => {
      if(data === 'bad_request'){
        alert('Ocorreu um problema ao tentar deletar o item, tente novamente');
      }
    })
    this.apiService.senderResult.subscribe(data => {
      if(data === 'delete_history'){
        console.log('Historico deletado com sucesso');
      }
    })
  }
  
}
