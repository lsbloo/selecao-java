
export class Historico{


    id: number
    type: string
    bandeira: string
    codigo_instalacao: string
    data_coleta:string
    estado: string
    municipio: string
    produto: string
    regiao: string
    revenda: string
    unidade_medida: string
    valor_venda: string
    valor_compra: string

    constructor(
        id: number,
        type: string,
        codigo_instalacao: string, 
        data_coleta: string,
        estado: string,
        municipio: string, 
        produto: string, 
        regiao: string, 
        revenda: string, 
        unidade_medida: string , 
        bandeira: string, 
        valor_venda: string, 
        valor_compra: string)
        {
            this.id=id
            this.type=type
            this.codigo_instalacao = codigo_instalacao
            this.data_coleta=data_coleta
            this.estado=estado
            this.municipio=municipio
            this.produto=produto
            this.regiao=regiao
            this.revenda=revenda
            this.bandeira=bandeira
            this.unidade_medida=unidade_medida
            this.valor_venda=valor_venda
            this.valor_compra=valor_compra
        }
}