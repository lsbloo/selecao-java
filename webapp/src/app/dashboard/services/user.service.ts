import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/credencial/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apiService: ApiService) {

   }

   findAllUsers(token_user: string): void{
     this.apiService.findAllUser(token_user);
   }

   deleteUser(id_user: number , token_user: string){
     this.apiService.deleteUser(id_user,token_user);
     
   }
}
