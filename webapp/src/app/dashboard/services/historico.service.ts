import { Injectable } from '@angular/core';
import {ApiService} from '../../credencial/services/api.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Historico } from '../historico/models/Historico';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  constructor(private apiService: ApiService, router: Router) {
   }


   addCsv(user_token: string , file_csv: FormData): boolean {
      let result = this.apiService.importCsvUser(user_token,file_csv);
     return false;
   }

   getResultParamData(param: string , page: number,data_coleta: string, token_user: string, param_type: string): Observable<Historico[]>{
     let search = param.toUpperCase();
     return this.apiService.getResultParamData(search, page,data_coleta, token_user,param_type);
   }

   getResultParamRevenda(param: string , page: number,data_coleta: string, token_user: string,param_type: string): Observable<Historico[]>{
    let search = param.toUpperCase();
    return this.apiService.getResultParamRevenda(search, page,data_coleta, token_user,param_type);
  }
  getResultParamSigla(param: string , page: number,data_coleta: string, token_user: string,param_type: string): Observable<Historico[]>{
    let search = param.toUpperCase();
    return this.apiService.getResultParamSigla(search, page,data_coleta, token_user,param_type);
  }

  getResultParamPriceCity(param: string ,data_coleta: string, token_user: string,param_type: string): Observable<Historico[]>{
    let search = param.toUpperCase();
    return this.apiService.getResultParamPriceCity(search,data_coleta, token_user,param_type);
  }
  
  getResultParamCity(param: string ,data_coleta: string, token_user: string,param_type: string): Observable<Historico[]>{
    let search = param.toUpperCase();
    return this.apiService.getResultParamCity(search,data_coleta, token_user,param_type);
  }
  
  getResultParamFlag(param: string ,data_coleta: string, token_user: string,param_type: string): Observable<Historico[]>{
    let search = param.toUpperCase();
    return this.apiService.getResultParamFlag(search,data_coleta, token_user,param_type);
  }


   deleteHistory(id_history: number, token_user: string){
    this.apiService.deleteHistory(id_history,token_user);
  }
}
