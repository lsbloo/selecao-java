import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';

import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './credencial/login/login.component';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import { CadastroComponent } from './credencial/cadastro/cadastro.component';
import {PainelComponent} from './dashboard/painel/painel.component';
import {UsuariosComponent} from './dashboard/usuarios/usuarios.component';
import { HistoricoComponent } from './dashboard/historico/historico.component';

/**
 *  Define as rotas e subrotas default da aplicação.
 *  Ainda não implementado o serviço de "guardas".
 * @author osvaldoairon
 */
const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'cadastro', component: CadastroComponent},
  {path: 'painel', component: PainelComponent
          , children: [
            {path: 'usuarios', component: UsuariosComponent},
            {path: 'historico', component: HistoricoComponent}
          ]},
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
   RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
